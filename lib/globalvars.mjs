// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import 'dotenv/config'

var Globals = {
    'serverPort':typeof process.env.COORDINATOR_PORT!==undefined?process.env.COORDINATOR_PORT:8080,
    'statisticsPort':typeof process.env.COORDINATOR_STATISTICS_PORT!==undefined?process.env.COORDINATOR_STATISTICS_PORT:8081,
    'apiServer':typeof process.env.COORDINATOR_API_SERVER!==undefined?process.env.COORDINATOR_API_SERVER:'',
    'apiHeader':typeof process.env.COORDINATOR_API_HEADER!==undefined?process.env.COORDINATOR_API_HEADER:'',
    'saveTime':typeof process.env.COORDINATOR_SAVE_TIME!==undefined?process.env.COORDINATOR_SAVE_TIME:60,
    'gcTime':typeof process.env.COORDINATOR_GC_TIME!==undefined?process.env.COORDINATOR_GC_TIME:60,
    'statisticsTime':typeof process.env.COORDINATOR_STATISTICS_TIME!==undefined?process.env.COORDINATOR_STATISTICS_TIME:60,

}
export default Globals;