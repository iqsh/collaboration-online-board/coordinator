// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later
import { fetchData } from '../database/request.mjs';

async function createDoc(backend, id){
    let connection = backend.connect();
    let doc = connection.get('boards', id);
    //TODO: check if data exists
    //console.log({doc});
    doc.fetch(function(error) {
        if (error){
            console.error(error);
        }
        fetchData(id).then(data=>{
            if(data && doc!==null && doc!==undefined){
                doc.create(data, (err)=>{});
                return;
            }else{
                console.error("Document could't be created!");
            }
        }).catch(error=>console.error(error));
    });
};

export { createDoc };