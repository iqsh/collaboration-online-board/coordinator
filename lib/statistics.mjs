// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import pidusage from 'pidusage';
import v8 from 'v8';

async function getServerStatistics(backend){
    try{
        const stat = await pidusage(process.pid);
        let object = {
            timestamp: new Date().toISOString(),
            cpu: Number(stat.cpu).toFixed(2),
            memory: Number(stat.memory / 1024 / 1024).toFixed(2),
            heap: Number(v8.getHeapStatistics().used_heap_size / 1024 / 1024).toFixed(2),
            response_time: null,
            requests_per_second: null,
            user_count: backend.pubsub.streamsCount,
            pane_count: Object.keys(backend.pubsub.streams).length,
        };
        return object;
    }
    catch(error){
        console.error(error);
    }
};

export { getServerStatistics }