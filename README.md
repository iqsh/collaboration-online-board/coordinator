### Deutsch
## Collaborative Online Board - Coordinator
Ein Websocket-Server, welche die Daten zwischen den *Oberfläche* koordiniert. Die Software nutzt das NodeJS *HTTP*-Modul und *ws* als Grundlage und *ShareDB* als *Echtzeit Datenbank Backend, welches auf Operationale Transformation (OT) basiert*.


### System Anforderungen:
* NodeJS 16+ 
* NPM
* Libc6 (or libc6-compat)
* Getestet mit *Ubuntu 20.04 LTS* und *Alpine Linux*

### Installation von den Software-Abhängigkeiten:
Gehen Sie in den *coordinator*-Ordner und führen Sie `npm install` aus.

### Ausführen der Anwendung:
Kopieren und benennen Sie die .env.example in .env um und passen Sie die Werte an.  
Zum Ausführen des Servers: `npm run start`

***

### English
## Collaborative Online Board - Coordinator
Websocket-Server coordinates the data of the *panes* between the users. The software uses the NodeJS *HTTP*-Module and *ws* as base and *ShareDB* as *Realtime database backend which is based on Operational Transform (OT)*. 


### System Requirements:
* NodeJS 16+ 
* NPM
* Libc6 (or libc6-compat)
* Tested with *Ubuntu 20.04 LTS* and *Alpine Linux*

### Installation of the software dependencies:
Go into the *coordinator* folder and run `npm install`.  

### Run application:
Copy and rename .env.example to .env and adapt the values.  
Run Server: `npm run start`

