// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later
import { parse } from 'url';
import { WebSocketServer } from 'ws';
import { createServer } from 'http';
import ShareDB from 'sharedb';
import WebSocketJSONStream from '@teamwork/websocket-json-stream';
import { ToadScheduler, SimpleIntervalJob, Task } from 'toad-scheduler';
import { saveJob, serverStatisticJob } from './database/save.mjs';
import Globals from './lib/globalvars.mjs';
import { createDoc } from './lib/document.mjs';
import { getServerStatistics } from './lib/statistics.mjs';

//ShareDB.types.register(json1.type);
let backend = new ShareDB();
backend.use('receive',(request, next)=>{
    let data = request.data || {};
    switch(data.a){
        case "heartbeat": break;
        default: next(); break;
    }
});
const scheduler = new ToadScheduler();

const server = createServer(async(req, res)=>{
    if(req.method === 'GET' && req.url === "/stats"){
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        const data = await getServerStatistics(backend);
        res.end(JSON.stringify(data));
    } else {
        res.statusCode = 404;
        res.end('Not Found');
    }
});

const start = async () => {
    try {
        server.listen(Globals.statisticsPort, () => {
            console.log(`HTTP-Server started 0.0.0.0:${Globals.statisticsPort}`);
        });
        server.on('error', (error)=>{
            console.error(error);
        });
        new WebSocketServer({port: Globals.serverPort, host: '0.0.0.0'}).on('connection', (ws, request) =>{
            ws.on('error', (err)=>{
                console.error(err);
            });
            const id = parse(request.url).pathname.replace(/\//g,"");
            try{
                if(ws.readyState===0 || ws.readyState===1){
                    let stream = new WebSocketJSONStream(ws);
                    stream.on('error', (error)=>{
                        console.error(error);
                    });
                    backend.listen(stream);
                    createDoc(backend, id);
                }
            }catch(error){
                console.error(error);
            }
        });
        const task = new Task('savejob', ()=> saveJob(backend));
        const job = new SimpleIntervalJob({ seconds: Number(Globals.saveTime), }, task);
        scheduler.addSimpleIntervalJob(job);

        const task2 = new Task('gc', ()=> garbageCollector());
        const job2 = new SimpleIntervalJob({ seconds: Number(Globals.gcTime), }, task2);
        scheduler.addSimpleIntervalJob(job2);

        const task3 = new Task('serverStatisticJob', async ()=> await serverStatisticJob(backend));
        const job3 = new SimpleIntervalJob({ seconds: Number(Globals.statisticsTime), runImmediately: true }, task3);
        scheduler.addSimpleIntervalJob(job3);
        console.log(`WS-Server started 0.0.0.0:${Globals.serverPort}`);
    } catch (err) {
        //fastify.log.error(err)
        console.error(err);
        scheduler.stop()
        process.exit(1)
    }
}

const garbageCollector = () => {
    var activePanes = [];
    var allPanes = [];
    if(backend.pubsub.streams){
        activePanes = Object.keys(backend.pubsub.streams).map(x=>x.replace("boards.",""));
    }
    if(backend.db.docs.boards){
        allPanes = Object.keys(backend.db.docs.boards);
    }
    var difference = allPanes.filter(x => activePanes.indexOf(x)=== -1);
    difference.forEach(element=>{
        delete backend.db.docs.boards[element];
    });
}


start();