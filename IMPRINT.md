Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
Verantwortliche nach § 18 Medienstaatsvertrag  
Dr. Gesa Ramm  
Schreberweg 5, 24119 Kronshagen   
http://www.iqsh.schleswig-holstein.de  
https://twitter.com/_IQSH  
+49 431 5403-0  
Das IQSH ist laut Satzung eine dem Bildungsministerium unmittelbar nachgeordnete, nicht rechtsfähige Anstalt des öffentlichen Rechts.
