// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import pidusage from 'pidusage';
import v8 from 'v8';
import { saveData, saveStatistics } from './request.mjs';
import { getServerStatistics } from '../lib/statistics.mjs';

function saveJob(backend){
    if(backend.db.docs.boards) {
        for (let key in backend.db.docs.boards) {
            saveData(key, backend.db.docs.boards[key].data);
        }
    }
}

async function serverStatisticJob(backend){
    const object = await getServerStatistics(backend);
    saveStatistics(object);
}

export { serverStatisticJob, saveJob }