// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import got from 'got';
import Globals from '../lib/globalvars.mjs';

const requestOptions = {
    retry:{
        limit: 10,
        methods: ['GET','POST'],
    },
    timeout: {
		lookup: 100,
		connect: 10000,
		secureConnect: 10000,
		socket: 10000,
		send: 10000,
		response: 10000
	}
}

const hookOptions = {
    beforeRetry: [
        (error, retryCount)=>{
            console.log("Retrying...");
            console.log(`Retrying [${retryCount}]: ${error.code}`);
        }
    ]
}

async function fetchData(id=null){
    try{
        let { success, data } = await got(Globals.apiServer+'boarddata/'+id, requestOptions).json();
        if(success)
            return data;
        return false;
    }catch(error){
        console.error(error);
        return false;
    }
}

async function saveData(id=null, data={}){
    let localOptions = JSON.parse(JSON.stringify(requestOptions));
    localOptions.hooks = hookOptions;
    localOptions.json = { data };
    try{
        let { success } = await got.post(Globals.apiServer+'boarddata/'+id, localOptions).json();
        return success;
    }catch(error){
        console.error(error);
        return false;
    }
}

async function saveStatistics(data={}){
    let localOptions = JSON.parse(JSON.stringify(requestOptions));
    localOptions.hooks = hookOptions;
    localOptions.json = data;
    localOptions.retry.limit = 1;
    try{
        let { success } = await got.post(Globals.apiServer+'stats', localOptions).json();
        return success;
    }catch(error){
        console.error(error);
        return false;
    }
}

export { fetchData, saveData, saveStatistics };